/**
 * @author PREVOT Kylian
 * JS File which returns current year for the copyright
 * into the footer.
 */

/**
 * @constant copyright
 * @type {HTMLParagraphElement | undefined}
 */
const copyright = document.getElementById("copyright");

if (copyright) {
  copyright.innerText = `©️ ${new Date().getFullYear()}. Tous droits réservés.`
}
